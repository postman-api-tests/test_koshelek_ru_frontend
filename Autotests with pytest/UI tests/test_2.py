import requests
import testVariables
import json
import re
from playwright.sync_api import Playwright, sync_playwright, expect


def test_run(playwright: Playwright) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://gitlab.com/")
    assert page.url == "https://about.gitlab.com/"
    print(page.url)
    print("test message 2")
    # assert 1 == 5
    # ---------------------
    context.close()
    browser.close()


# with sync_playwright() as playwright:
#     test_run(playwright)
